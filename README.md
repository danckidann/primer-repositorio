# Curso de Git :D!
 
Este repositorio tiene como fin, realizar las practicas vistas en el Cruso de Git en https://gep.infotec.mx
 
## Clonado de este repositorio
 
```console
git clone https://gitlab.com/jorge.cervantes.silva/lineamientos.git
```
 
## Generacion de una rama
 
```console
git checkout -b NOMBRE_RAMA
```
 
 
--------------------------------------------------------
## Crear un nuevo repositorio en Gitlab.com
 
En la pagina de inicio seleccionar nuevo proyecto:
 
![nuevo proyecto](./img/nuevo-proy.png)
 
Despues seleccionar la opción de Crear proyecto en blanco:
 
![nuevo proyecto](./img/config-proy.png)
 
## Clonado de repositorio
 
```console
git clone https://gitlab.com/URL_DE_NUEVO_REPOSITORIO
```
--------------------------------------------------------
 
## Copiar los archivos de una carpeta a otra:
 
```console
cp ARCHIVO_O_CARPETA_A_COPIAR UBICACION_DONDE_SE_QUIERE_COPIAR
```
 
## Agregar/preparar TODOS los cambios en un proyecto
 
```console
git add ARCHIVOS
```
 
## Confirmar cambios Agregados/preparados
 
```console
git commit -m "MENSAJE PERSONALIZADO DEL COMMIT"
```
 
## Publicación en repositorio remoto de cambios recién confirmados
 
```console
git push origin RAMA_DESTINO
```
>Nota: se recomiend que el primero push de un proyecto vaya a la rama_destino **master**

| Nombre          | Repositorio    | 
|-------------------|-------------|
|Danny Tellez     |   https://gitlab.com/danckidann/primer-repositorio.git |  
|          |        | 
|           |       | 
|           |       | 
