package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	private static void testHU10(String valor){
		System.out.println("Esto es test para la prueba de la HU-10");
	}

	private static void cambioHU21(String valor){
		System.out.println("Esto es un cambio hecho en la rama HU-21");
	}
	
	private static void cambioTR02(String valor){
		System.out.println("Esto es un cambio hecho en la rama TR-02");
	}
}
