Changelog

agrego un coment a mi changelong.md en mi rama bug

Todos los cambios notables a este proyecto serán documentados es este archivo.

El formato esta basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), mantenemos las versiones de acuerdo
a [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

**Dada una versión número MAJOR.MINOR.PATCH:** (aplica a partir de 2.0.0)

* Se incrementará **MAJOR**, cuando se implemente un nuevo tipo de **Módulo** que genere incompatibilidad con versión
  anterior.
* Se incrementa **MINOR**, cuando se agregue una o más **nueva(s) funcionalidad(es) a la aplicación**.
* Se incrementa **PATCH**, cuando se implemente un **fix**.



## [Released]

## [0.8.2] - 2022-06-16
### [fix]
* [HU-LINEA-1233](https://openproject-gep.infotec.mx/projects/test/work_packages/1233/activity) - Ejemplo Test. 

## [0.8.1] - 2022-02-28
### [fix]
* [TR-LINEA-02](https://openproject.infotec.mx/projects/test/work_packages/02/activity) - Se modifican estilos de codigo. 

## [0.8.0] - 2022-02-28
### [add]
* [HU-LINEA-21](https://openproject.infotec.mx/projects/test/work_packages/05/activity) - Historia de Usuario 21 : Se modifica el archivo  src/main/java/com/example/demo/DemoApplication.java


## [0.7.0] - 2022-02-23
### [add]

* [HU-LINEA-23](https://openproject.infotec.mx/projects/test/work_packages/23/activity) - Historia de Usuario 23
## [0.6.0] - 2022-02-23
### [add]

* [HU-LINEA-05](https://openproject.infotec.mx/projects/test/work_packages/05/activity) - Historia de Usuario 05 : Genero header.txt como prueba


## [0.5.2] - 2022-02-23
## [fix]
* [HU-LINEA-13](https://openproject.infotec.mx/projects/test/work_packages/13/activity) - Historia de Usuario 13.
## [0.5.1] - 2022-02-23

### [fix]

* [HU-LINEA-14](https://openproject.infotec.mx/projects/test/work_packages/14/activity) - Historia de Usuario 14.

## [0.5.0] - 2022-02-23

### [add]

* [HU-LINEA-10](https://openproject.infotec.mx/projects/test/work_packages/10/activity) - Historia de Usuario 10.

## [0.4.0] - 2022-02-22
### [add]

* [HU-LINEA-02](https://openproject.infotec.mx/projects/test/work_packages/02/activity) - Historia de Usuario 2.


## [0.3.0] - 2022-02-21

### [add]

* [HU-LINEA-21](https://openproject.infotec.mx/projects/test/work_packages/124/activity) - O
Se crean directorios para flujo de trabajo de CI/CD 21.

## [0.2.0] - 2022-02-09

### [add]

* [HU-LINEA-01](https://openproject.infotec.mx/projects/test/work_packages/124/activity) - Historia de Usuario 1.

## [0.1.0] - 2022-02-08
### [add]
* [HU-LINEA-05](https://openproject.infotec.mx/projects/aamates-desarrollo/) - Historia de Usuario 5

## [0.0.2] - 2022-02-04

### [add]

* [HU-LINEA-07](https://openproject.infotec.mx/projects/aamates-desarrollo/) - Se crean directorios para flujo de
  trabajo de CI/CD.
  